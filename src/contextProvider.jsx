import React, { useEffect, useReducer, useState } from "react"
import {getArrayProducts} from "./services/products"
import { getUser } from "./services/user";
import {sortByNameAZ,sortByNameZA, sortHigherPrice, sortLowerPrice} from "../src/utils/sortsfunctions"
import { redeem } from "./services/redeem";
import { getPoints } from "../src/services/coins"
export const AppContext = React.createContext();

export default function AppProvider({ children }) {

const [arrayProducts, setArrayProducts] = useState([])
const [userInfo, setUserInfo] = useState([])
const [open, setOpen] = useState(false)
const [points,setPoints] = useState(0)
const [sortRender, setSortRender] = useState(true)

const handleSortByNameAZ = () => {
    setArrayProducts(sortByNameAZ(arrayProducts))
    setSortRender(!sortRender)
}

const handleSortByNameZA = () => {
    setArrayProducts(sortByNameZA(arrayProducts))
    setSortRender(!sortRender)
}

const handleSortHigherPrice = () => {
    setArrayProducts(sortHigherPrice(arrayProducts))
    setSortRender(!sortRender)
}
const handleSortLowerPrice= () => {
    setArrayProducts(sortLowerPrice(arrayProducts))    
    setSortRender(!sortRender)
}
const handleReset = () =>{
    getArrayProducts().then((arrayProds)=>setArrayProducts(arrayProds))
}
const handleReedem = (productId, cost) =>{
    if (cost < userInfo.points) {
    redeem(productId)
    let newPoints = points - parseInt(cost)
    setPoints(newPoints)}
    else {window.alert("No tienes los puntos suficientes")}
}
const handleGetPoints = (quanty) => {
    getPoints(quanty)
    let newPoints = points + quanty  
    setPoints(newPoints)
}
const handleSetInfoUser = (user) => {
    setUserInfo(user)
    setPoints(user.points)
}



useEffect (()=>{getArrayProducts().then((arrayProds)=>setArrayProducts(arrayProds))},[])
useEffect (()=>{getUser().then((user)=> handleSetInfoUser(user))},[])
               

        return (
        <AppContext.Provider value={{ handleReset,
            handleSortByNameAZ, 
            handleSortByNameZA,
            setArrayProducts,handleSortLowerPrice, 
            handleSortHigherPrice,
            arrayProducts,
            userInfo,
            setUserInfo,
            setOpen,
            open,
            setPoints,
            points,
            sortRender,
            setSortRender,
            handleGetPoints,
            handleReedem}}>{children}</AppContext.Provider>
    );
}