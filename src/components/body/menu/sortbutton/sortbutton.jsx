import './sortbutton.scss'

export function SortButton(props) {
        return <button className="sortbutton" onClick={props.handle}>
        {props.textButton}
	</button>
}
