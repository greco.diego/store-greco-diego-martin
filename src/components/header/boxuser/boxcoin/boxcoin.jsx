import './boxcoin.scss'
import { AppContext } from '../../../../contextProvider'

import React from "react"
export function BoxCoin() {
	const {points} = React.useContext(AppContext)
	return <div className='boxcoin'>
	{points}<img src="./images/icons/coin.svg" alt="coin" />
	</div>
}