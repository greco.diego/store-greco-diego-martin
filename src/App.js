import './app.scss'
import {Header} from "./components/header/header.jsx"
import {Banner} from"./components/banner/banner.jsx"
import {Body} from"./components/body/body.jsx"
import React from "react"
function App() {
	return <div className='App'>
			<Header/>
			<Banner/> 
			<Body/>
	</div>
}
export default App
